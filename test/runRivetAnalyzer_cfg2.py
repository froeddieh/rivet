import FWCore.ParameterSet.Config as cms

process = cms.Process("runRivetAnalysis")

process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = cms.untracked.int32(5000)

process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1)
)
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
		#'file:/home/schaaf/storage/AODSIM/MC_MadGraph_TTbar_8TeV_AODSIM.root',
		'file:/home/schaaf/storage/AODSIM/MC_MadGraph_TTbar_8TeV_AODSIM_2.root',
		#'file:/home/schaaf/storage/AODSIM/MC_MadGraph_TTbar_8TeV_AODSIM_3.root',
		#'file:/home/schaaf/storage/AODSIM/MC_MadGraph_TTbar_8TeV_AODSIM_4.root',
		#'file:/home/schaaf/storage/AODSIM/cummu.root',
	)
	#fileNames = cms.untracked.vstring('file:/home/schaaf/storage/AODSIM/MC_MadGraph_TTbar_8TeV_AODSIM_3.root')
)

process.load("GeneratorInterface.RivetInterface.rivetAnalyzer_cfi")
process.load("SimGeneral.HepPDTESSource.pythiapdt_cfi")
process.load("GeneratorInterface.RivetInterface.genParticles2HepMC_cfi")

#process.rivetAnalyzer.AnalysisNames = cms.vstring('MC_LES_HOUCHES_SYSTEMATICS_CMS','CMS_EWK_10_012')
process.rivetAnalyzer.AnalysisNames = cms.vstring('TTBarLJet')

#process.p = cms.Path(process.rivetAnalyzer)
process.p = cms.Path(process.generator*process.rivetAnalyzer)

