#!/bin/sh

export RivetAnalysis=TTBarLJet
export RivetDataSample=$Home/storage/hepmc/MC_MadGraph_TTbar_7TeV_TuneZ2_PU_S6_START42_V14B-v2_AODSIM.hepmc

scram b -j16
rivet -a $RivetAnalysis $RivetDataSample

#root -l scripts/root