// -*- C++ -*-
// Author: F. Schaaf
// Created: 2014-09-02
//

#include "Rivet/Analysis.hh"
#include "Rivet/Rivet.hh"
//#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
//#include "Rivet/ParticleName.hh"

#include "HepMC/GenEvent.h"

// #include "TH1F.h"
//#include "TFile.h"
// #include "TFile.h"

// #include "TH2F.h"
// #include <TH1D.h>
// #include <TH2D.h>
// #include <TNtuple.h>
// #include <TROOT.h>
// #include <TSystem.h>
// #include <TString.h>
// #include <TCanvas.h>
// #include <TVector3.h>
// #include <TRandom.h>


namespace Rivet {


////////////
// Config //
////////////

// Root
// const char* ROOT_FILE_NAME( "analysis.root" );

// Lepton Cuts
const double MIN_PT_GOOD_ELECTRON = 30*GeV;
const double MIN_PT_GOOD_MUON = 30*GeV;
const double MIN_PT_LOOSE_ELECTRON = 20*GeV;
const double MIN_PT_LOOSE_MUON = 10*GeV;

const double MAX_ETA_GOOD_ELECTRON = 2.1;
const double MAX_ETA_GOOD_MUON = 2.1;
const double MAX_ETA_GOOD_TAU = 2.1;
const double MAX_ETA_LOOSE_ELECTRON = 2.1;
const double MAX_ETA_LOOSE_MUON = 2.1;
const double MAX_ETA_LOOSE_TAU = 2.1;

// Jet Cuts
const double JET_MIN_PT = 30 * GeV;
const double JET_MAX_ETA = 2.5;
const double JET_MIN_DELTA_R = 0.5;


class CMS_2014_ttbarPlusXJets : public Analysis
{
public:

  CMS_2014_ttbarPlusXJets() : Analysis( "CMS_2014_ttbarPlusXJets" ), _nameLength(18), _nJets(20)
  {
  }

private:

  enum Cuts
  {
    TotalEvents=0,
    GoodLepton,
    GoodLepton_Electron,
    GoodLepton_Muon,
    LooseLeptonVeto,
    LooseLeptonVeto_Electron,
    LooseLeptonVeto_Muon,
    NJetsGE4,
    NJetsGE4_Electron,
    NJetsGE4_Muon,
    NBJetsGE2,
    NBJetsGE2_Electron,
    NBJetsGE2_Muon,
    Size
  };

  char** _cutNames;

  size_t* _cuts;

//   class Root
//   {
// 
//   private:
//     TFile* tFile;
// 
//   public:
// 
//     TH1F* nJets;
//     TH1F* nBJets;
//     TH1F* nJetsLessThan4;
//     TH1F* nJetsWrongEta;
//     TH1F* nJetsTooCloseToGoodLepton;
// 
//     Root()
//     {
//       tFile = new TFile( ROOT_FILE_NAME, "recreate" );
//       nJets = new TH1F( "nJets","nJets",7,4,10 );
//       nBJets = new TH1F( "nBJets","nBJets",10,0,10 );
//       nJetsLessThan4 = new TH1F( "nJetsLessThan4","nJetsLessThan4",4,0,3);
//       nJetsWrongEta = new TH1F( "nJetsWrongEta","nJetsWrongEta",10,0,10 );
//       nJetsTooCloseToGoodLepton = new TH1F( "nJetsTooCloseToGoodLepton","nJetsTooCloseToGoodLepton",10,0,10 );
//     }
// 
//     ~Root()
//     {
//       tFile->Write();
//     }
// 
//   };
// 
//   Root* _root;

private:

  const size_t _nameLength;

  void init_Names()
  {

    _cuts = new size_t[Size];

        _cutNames = new char* [Size];

    for (size_t i = 0; i<Size; i++)
    {
            _cutNames[i] = new char[_nameLength];
    }
    sprintf(_cutNames[0], "TotalEvents       ");
    sprintf(_cutNames[1], "GoodLepton        ");
    sprintf(_cutNames[2], "GoodLepton|e      ");
    sprintf(_cutNames[3], "GoodLepton|mu     ");
    sprintf(_cutNames[4], "LooseLeptonVeto   ");
    sprintf(_cutNames[5], "LooseLeptonVeto|e ");
    sprintf(_cutNames[6], "LooseLeptonVeto|mu");
    sprintf(_cutNames[7], "nJets>=4          ");
    sprintf(_cutNames[8], "nJets>=4|e        ");
    sprintf(_cutNames[9], "nJets>=4|mu       ");
    sprintf(_cutNames[10],"nBJets>=2         ");
    sprintf(_cutNames[11],"nBJets>=2|e       ");
    sprintf(_cutNames[12],"nBJets>=2|mu      ");
  }

  void init_Projections()
  {
    IdentifiedFinalState ifsLeptons;
    ifsLeptons.acceptIdPair( ELECTRON );
    ifsLeptons.acceptIdPair( MUON );
    ifsLeptons.acceptIdPair( NU_E );
    ifsLeptons.acceptIdPair( NU_MU );
    addProjection( ifsLeptons, "Leptons" );
    const FinalState finalState;
    addProjection( FastJets( finalState, FastJets::ANTIKT, 0.5 ), "Jets" );
  }

  void init_Output()
  {
    for (size_t i = 0; i<Size; i++)
    {
            _cuts[i] = 0;
    }
  }

//   void init_Root()
//   {
//     _root = new Root();
//   }

public:

  void init()
  {
    init_Names();
    init_Projections();
    init_Output();
//     init_Root();
  }

private:
	
  std::vector<int> _nJets;  

  bool isLepton( Particle const& p )
  {
    int const& absid = abs( p.pdgId() );
    return absid == ELECTRON || absid == MUON || absid == NU_E || absid == NU_MU;
  }

  enum ParticleTypeMatch
  {
    EXACT,
    ALLOW_SYMMETRIC_PARTNER
  };

  bool isParticleType( Particle const& p, int type, const ParticleTypeMatch particleTypeMatch = ALLOW_SYMMETRIC_PARTNER )
  {
    int pid = p.pdgId();
    if( particleTypeMatch == ALLOW_SYMMETRIC_PARTNER )
    {
      pid = abs( pid );
      type = abs( type );
    }
    return pid == type;
  }

  bool isE;
  bool isMu;

  bool analyze_Lepton( const Event& event, const IdentifiedFinalState& leptons, Particle& chosenLepton )
  {
    vector<Particle> goodElectrons;
    vector<Particle> goodMuons;
    vector<Particle> looseElectrons;
    vector<Particle> looseMuons;
    foreach( const Particle& p, leptons.particlesByPt() )
    {
      double pT = p.momentum().pT();
      double absEta = abs( p.momentum().eta() );
      if( isParticleType( p, ELECTRON ) && pT > MIN_PT_GOOD_ELECTRON && absEta < MAX_ETA_GOOD_ELECTRON )
      {
        goodElectrons.push_back( p );
      }
      else if( isParticleType( p, MUON ) && pT > MIN_PT_GOOD_MUON && absEta < MAX_ETA_GOOD_MUON )
      {
        goodMuons.push_back( p );
      }
      if( isParticleType( p, ELECTRON ) && pT > MIN_PT_LOOSE_ELECTRON && absEta < MAX_ETA_LOOSE_ELECTRON )
      {
        looseElectrons.push_back( p );
      }
      else if( isParticleType( p, MUON ) && pT > MIN_PT_LOOSE_MUON && absEta < MAX_ETA_LOOSE_MUON )
      {
        looseMuons.push_back( p );
      }
    }

    // Good Lepton Cut
    bool goodElectron = goodElectrons.size()==1 && goodMuons.size() == 0;
    bool goodMuon = goodMuons.size()==1 && goodElectrons.size() == 0;
    bool goodLepton = goodElectron | goodMuon;
    if (goodLepton)
    {
      _cuts[GoodLepton]++;
    }

    if (goodElectron)
    {
      _cuts[GoodLepton_Electron]++;
    }

    if (goodMuon)
    {
      _cuts[GoodLepton_Muon]++;
    }

    // loose Lepton Veto
    isE = goodElectrons.size() == 1 && looseElectrons.size() == 1 && looseMuons.size() == 0;
    isMu = goodMuons.size() == 1 && looseElectrons.size() == 0 && looseMuons.size() == 1;
    bool isSelected = isE ^ isMu;

    if (!isSelected)
    {
      return false;
    }

    if( isE )
    {
      _cuts[LooseLeptonVeto_Electron]++;
      chosenLepton = goodElectrons[0];
    }
    if( isMu )
    {
      _cuts[LooseLeptonVeto_Muon]++;
      chosenLepton = goodMuons[0];
    }
    return true;
  }

  bool analyze_CountJets( const Event& event, const Particle chosenLepton )
  {
    vector<FourMomentum> jets;
    int nBJets( 0 );
    int nJetsWrongEta( 0 );
    int nJetsTooCloseToGoodLepton(0);
    foreach( const Jet& j, applyProjection<FastJets>( event, "Jets" ).jetsByPt( JET_MIN_PT ) )
    {
      const double absEta = abs( j.momentum().eta() );
      if( absEta > JET_MAX_ETA )
      {
    nJetsWrongEta++;
    continue;
      }
      if ( deltaR( chosenLepton.momentum(), j.momentum() ) < JET_MIN_DELTA_R )
      {
    nJetsTooCloseToGoodLepton++;
    continue;
      }

      jets.push_back( j.momentum() );
      if( j.containsBottom() )
      {
    nBJets++;
      }
    }

    int nJets = jets.size();

//     _root->nJetsTooCloseToGoodLepton->Fill( nJetsTooCloseToGoodLepton );
//     _root->nJetsWrongEta->Fill( nJetsWrongEta );

    if (nJets < 4)
    {
//       _root->nJetsLessThan4->Fill( nJets );
      return false;
    }

    _cuts[NJetsGE4]++;
    if (isE) _cuts[NJetsGE4_Electron] ++;
    if (isMu) _cuts[NJetsGE4_Muon] ++;


//     _root->nBJets->Fill( nBJets );

    if (nBJets < 2)
    {
      return false;
    }

//     _root->nJets->Fill( min(10,nJets) );

    _nJets[min(10,nJets)] ++;

    return true;
  }

public:

  void analyze( const Event& event )
  {
    _cuts[TotalEvents]++;
    const IdentifiedFinalState leptons = applyProjection<IdentifiedFinalState>( event, "Leptons" );
    Particle chosenLepton;
    if( !analyze_Lepton( event, leptons, chosenLepton ) )
    {
      vetoEvent;
    }
    _cuts[LooseLeptonVeto]++;

    if (!analyze_CountJets( event, chosenLepton ))
    {
      vetoEvent;
    }
    _cuts[NBJetsGE2]++;
    if (isE) _cuts[NBJetsGE2_Electron] ++;
    if (isMu) _cuts[NBJetsGE2_Muon] ++;
  }

//   void finalize_Root()
//   {
//     double scale = _root->nJets->GetEntries();
//     _root->nJets->Sumw2();
//     _root->nJets->Scale(1./scale);
//     delete _root;
//   }

  void finalize_Names()
  {

    for (size_t i = 0; i<Size; i++)
    {
            delete [] _cutNames[i];
    }
    delete [] _cutNames;
    delete [] _cuts;

}

  void finalize_DisplayCuts()
  {
    int count (-1);
    int countIndex( 0 );
    int childCount (0);

    cout << "[Cut flow]" << endl;
    cout << "--------------------------------------------------------" << endl;
    cout << "[#.#] (Cut Descr.|Branch):nEvents ( Parent% /   Total% )" << endl;
    cout << "--------------------------------------------------------" << endl;

    for( size_t i=0; i<Size; i++ )
    {
      double percentage = 100.*_cuts[i]/_cuts[TotalEvents];
      double ofParentPercentage = 100. * _cuts[i]/_cuts[countIndex];
      cout.precision( 3 );

      bool isChild = false;
      if (std::string(_cutNames[i]).find('|') != std::string::npos)
      {
    isChild = true;
      }

      if (!isChild)
      {
    count++;
    countIndex = i;
      }

      cout << "[" << count;

      if (isChild)
      {
    cout << "." << childCount;
      }
      else
      {
    cout << "  ";
      }
      cout << "] (" << _cutNames[i] << "): " << setw(6) << _cuts[i] << " (" << setw(7) << ofParentPercentage << "% / " << setw(7) << percentage << "% )" << endl;

      if (!isChild)
      {
    childCount=0;
      }
      else
      {
    childCount++;
      }

    }
    cout << "--------------------------------------------------------" << endl;
  }

  void finalize_DisplayJetMultiplicy()
  {
    cout << "[Jet Multiplicy]" << endl;
    for (int i=4;i<=10;i++)
    {
      cout << "[" << i << "]: " << _nJets[i] << " +- " << sqrt((double)_nJets[i]) << endl;
    }
  }

  void finalize_DisplayResults()
  {
    cout << endl;
    cout << "[Displaying Results]" << endl;
    cout << endl;
    finalize_DisplayCuts();
    cout << endl;
    finalize_DisplayJetMultiplicy();
    cout << endl;
  }

  void finalize()
  {
    finalize_DisplayResults();
//     finalize_Root();
  }



};

/*

  class CMS_2014_ttbarPlusXJets_old : public Analysis {
  public:

    /// @name Constructors etc.
    //@{

    /// Constructor
    CMS_2014_ttbarPlusXJets_old()
      : Analysis("CMS_2014_ttbarPlusXJets")
    {    }

    //@}


  public:

    /// @name Analysis methods
    //@{

    /// Book histograms and initialise projections before the run
    void init() {

      /// @todo Initialise and register projections here

      /// @todo Book histograms here, e.g.:
      // _h_XXXX = bookProfile1D(1, 1, 1);
      // _h_YYYY = bookHistogram1D(2, 1, 1);

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {
      //const double weight = event.weight();

      /// @todo Do the event by event analysis here

    }


    /// Normalise histograms etc., after the run
    void finalize() {

      /// @todo Normalise, scale and otherwise manipulate histograms here

      // scale(_h_YYYY, crossSection()/sumOfWeights()); # norm to cross section
      // normalize(_h_YYYY); # normalize to unity

    }

    //@}


  private:

    // Data members like post-cuts event weight counters go here


  private:

    /// @name Histograms
    //@{
    AIDA::IProfile1D *_h_XXXX;
    AIDA::IHistogram1D *_h_YYYY;
    //@}


  };*/



  // The hook for the plugin system
  DECLARE_RIVET_PLUGIN(CMS_2014_ttbarPlusXJets);

}
