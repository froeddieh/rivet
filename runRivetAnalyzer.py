#!/usr/bin/env python

import os
import copy
import subprocess

nCores = 16
nCurrentCores = 0
processes = []
cmsRun = [ "cmsRun", "/home/schaaf/CMSSW_7_1_1/src/GeneratorInterface/RiverInterface/test/runRivetAnalyzer_cfg_arg.py" ]
#cmsRun = [ "ls" ]

dirList = []

directory = "/storage/9/descroix/UnSkimmedStuff/FredSamples"

for idx, fileName in enumerate(os.listdir(directory)):
        print str(idx) + ": " + fileName
	if nCurrentCores == nCores:
		for idx, p in enumerate(processes):
			print "Waiting for subProcess #" + str(idx)
			p.wait()
		processes = []
		nCurrentCores = 0
	thisProcessParams = copy.copy(cmsRun)
	thisProcessParams.append(directory + "/" + fileName)
	thisDir = "./" + str(idx)
	dirList.append(thisDir)
	if not os.path.exists(thisDir):
    		os.makedirs(thisDir)
	#print "running process:"
	#print thisProcessParams
	processes.append(subprocess.Popen(thisProcessParams,0,None,None,None,None,None,False,False,thisDir))
	nCurrentCores = nCurrentCores + 1
	
for idx, p in enumerate(processes):
	print "Waiting for subProcess #" + str(idx)
	p.wait()
processes = []

	
hAddList = []
hAddList.append("hadd")
hAddList.append("-f")
hAddList.append("analysis.root")

print "DirList="
print dirList

for dir in dirList:
	hAddList.append(dir + "/" + "analysis.root")
	
hAddProcess = subprocess.Popen(hAddList)
print "hAddList[] = " + str(hAddList)
print "Waiting for hadd to finish ..."
hAddProcess.wait()
