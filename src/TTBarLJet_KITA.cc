// -*- C++ -*-
// Author: F. Schaaf
// Created: 2014-09-02
//

#include "Rivet/Analysis.hh"
//#include "Rivet/RivetAIDA.hh"
//#include "Rivet/Tools/Logging.hh"
#include "Rivet/Rivet.hh"
//#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/IdentifiedFinalState.hh"
#include "Rivet/Tools/ParticleIdUtils.hh"
//#include "Rivet/ParticleName.hh"

#include "HepMC/GenEvent.h"

#include "TFile.h"
#include "TH1F.h"
//#include "TH2F.h"
// #include <TH1D.h>
// #include <TH2D.h>
// #include <TNtuple.h>
// #include <TROOT.h>
// #include <TSystem.h>
// #include <TString.h>
#include <TCanvas.h>
// #include <TVector3.h>
// #include <TRandom.h>

namespace Rivet
{

////////////
// Config //
////////////

//#define JET_RECO_DEBUG_OUTPUT

// Root
const char* ROOT_FILE_NAME_KITA( "analysis_kita.root" );

// Lepton Cuts
const double MIN_PT_GOOD_ELECTRON = 30*GeV;
const double MIN_PT_GOOD_MUON = 30*GeV;
const double MIN_PT_GOOD_TAU = 30*GeV;
const double MIN_PT_LOOSE_ELECTRON = 20*GeV;
const double MIN_PT_LOOSE_MUON = 10*GeV;
const double MIN_PT_LOOSE_TAU = 10*GeV;

const double MAX_ETA_GOOD_ELECTRON = 2.1;
const double MAX_ETA_GOOD_MUON = 2.1;
const double MAX_ETA_GOOD_TAU = 2.1;
const double MAX_ETA_LOOSE_ELECTRON = 2.1;
const double MAX_ETA_LOOSE_MUON = 2.1;
const double MAX_ETA_LOOSE_TAU = 2.1;

// Jet Cuts
const double JET_MIN_PT = 30 * GeV;
const double JET_MAX_ETA = 2.5;
const double JET_MIN_DELTA_R = 0.4;

const double JET_RECO_MIN_PT = 20 * GeV;

// Reco Masses
const double RECO_MASS_W_HAD = 80.4199;
const double RECO_MASS_SIGMA_W_HAD = 5.98447;
const double RECO_MASS_SIGMA_W_HAD2 = RECO_MASS_SIGMA_W_HAD* RECO_MASS_SIGMA_W_HAD;
const double RECO_MASS_T_HAD =172.736;
const double RECO_MASS_SIGMA_T_HAD = 7.79868;
const double RECO_MASS_SIGMA_T_HAD2 = RECO_MASS_SIGMA_T_HAD* RECO_MASS_SIGMA_T_HAD;
const double RECO_MASS_T_LEP =173.133;
const double RECO_MASS_SIGMA_T_LEP = 3.52318;
const double RECO_MASS_SIGMA_T_LEP2 = RECO_MASS_SIGMA_T_LEP* RECO_MASS_SIGMA_T_LEP;

class TTBarLJet_KITA : public Analysis
{
public:

    TTBarLJet_KITA() : Analysis( "TTBarLJet_KITA" ), _nameLength( 18 )
    {
    }

private:

    enum Cuts
    {
        TotalEvents=0,
        GoodLepton,
        GoodLepton_Electron,
        GoodLepton_Muon,
        LooseLeptonVeto,
        LooseLeptonVeto_Electron,
        LooseLeptonVeto_Muon,
        NJetsGE4,
        NJetsGE4_Electron,
        NJetsGE4_Muon,
        NBJetsGE2,
        NBJetsGE2_Electron,
        NBJetsGE2_Muon,
        Size
    };

    char** _cutNames;

    size_t* _cuts;

    class Root
    {

    private:
        TFile* tFile;

    public:

        TH1F* nJets;
        TH1F* nBJets;
        TH1F* nJetsLessThan4;
        TH1F* nJetsWrongEta;
        TH1F* nJetsTooCloseToGoodLepton;
        TH1F* chi;
        TH1F* chiHadW;
        TH1F* chiLepT;
        TH1F* chiHadT;
        TH1F* massLepW;
        TH1F* massHadW;
        TH1F* massLepT;
        TH1F* massHadT;
        TH1F* additionalJets;
        TH1F* additionalJets_chiCut;

        Root()
        {
            tFile = new TFile( ROOT_FILE_NAME_KITA, "RECREATE" );
            nJets = new TH1F( "nJets","nJets",6,3.5,9.5 );
            nBJets = new TH1F( "nBJets","nBJets",10,-0.5,9.5 );
            nJetsLessThan4 = new TH1F( "nJetsLessThan4","nJetsLessThan4",4,-0.5,3.5 );
            nJetsWrongEta = new TH1F( "nJetsWrongEta","nJetsWrongEta",10,-0.5,9.5 );
            nJetsTooCloseToGoodLepton = new TH1F( "nJetsTooCloseToGoodLepton","nJetsTooCloseToGoodLepton",10,-0.5,9.5 );
            chi = new TH1F( "chi","chi",100,-0.5,49.5 );
            chiHadW = new TH1F( "chiHadW","chiHadW",100,-0.5,49.5 );
            chiLepT = new TH1F( "chiLepT","chiLepT",100,-0.5,49.5 );
            chiHadT = new TH1F( "chiHadT","chiHadT",100,-0.5,49.5 );
            massLepW = new TH1F( "massLepW","massLepW",250,-0.5,295.5 );
            massHadW = new TH1F( "massHadW","massHadW",250,-0.5,295.5 );
            massLepT = new TH1F( "massLepT","massLepT",250,-0.5,495.5 );
            massHadT = new TH1F( "massHadT","massHadT",250,-0.5,495.5 );
            additionalJets = new TH1F( "additionalJets","additionalJets",3,-0.5,2.5 );
            additionalJets_chiCut = new TH1F( "additionalJets_chiCut","additionalJets_chiCut",3,-0.5,2.5 );
        }

        ~Root()
        {
            nJets->Write();
            nBJets->Write();
            nJetsLessThan4->Write();
            nJetsWrongEta->Write();
            nJetsTooCloseToGoodLepton->Write();
            chi->Write();
            chiHadW->Write();
            chiLepT->Write();
            chiHadT->Write();
            massLepW->Write();
            massHadW->Write();
            massLepT->Write();
            massHadT->Write();
            additionalJets->Write();
            additionalJets_chiCut->Write();
            tFile->Write();
        }

    };

    Root* _root;

private:

    const size_t _nameLength;

    void init_Names()
    {
        _cuts = new size_t[Cuts::Size];
        _cutNames = new char* [Cuts::Size];
        for( size_t i = 0; i<Cuts::Size; i++ )
        {
            _cutNames[i] = new char[_nameLength];
        }
        sprintf( _cutNames[0], "TotalEvents       " );
        sprintf( _cutNames[1], "GoodLepton        " );
        sprintf( _cutNames[2], "GoodLepton|e      " );
        sprintf( _cutNames[3], "GoodLepton|mu     " );
        sprintf( _cutNames[4], "LooseLeptonVeto   " );
        sprintf( _cutNames[5], "LooseLeptonVeto|e " );
        sprintf( _cutNames[6], "LooseLeptonVeto|mu" );
        sprintf( _cutNames[7], "nJets>=4          " );
        sprintf( _cutNames[8], "nJets>=4|e        " );
        sprintf( _cutNames[9], "nJets>=4|mu       " );
        sprintf( _cutNames[10],"nBJets>=2         " );
        sprintf( _cutNames[11],"nBJets>=2|e       " );
        sprintf( _cutNames[12],"nBJets>=2|mu      " );
    }

    void init_Projections()
    {
        IdentifiedFinalState ifsLeptons;
        ifsLeptons.acceptIdPair( ELECTRON );
        ifsLeptons.acceptIdPair( MUON );
        ifsLeptons.acceptIdPair( NU_E );
        ifsLeptons.acceptIdPair( NU_MU );
        addProjection( ifsLeptons, "Leptons" );
        const FinalState finalState;
        addProjection( FastJets( finalState, FastJets::ANTIKT, 0.5 ), "Jets" );
    }

    void init_Output()
    {
        for( size_t i = 0; i<Cuts::Size; i++ )
        {
            _cuts[i] = 0;
        }
    }

    void init_Root()
    {
        _root = new Root();
    }

public:

    void init()
    {
        init_Names();
        init_Projections();
        init_Output();
        init_Root();
    }

private:

    enum ParticleTypeMatch
    {
        EXACT,
        ALLOW_SYMMETRIC_PARTNER
    };

    bool isParticleType( Particle const& p, int type, const ParticleTypeMatch particleTypeMatch = ALLOW_SYMMETRIC_PARTNER )
    {
        int pid = p.pdgId();
        if( particleTypeMatch == ALLOW_SYMMETRIC_PARTNER )
        {
            pid = abs( pid );
            type = abs( type );
        }
        return pid == type;
    }

    bool isE;
    bool isMu;

    bool analyze_Lepton( const Event& event, const IdentifiedFinalState& leptons, Particle& chosenLepton )
    {
        vector<Particle> goodElectrons;
        vector<Particle> goodMuons;
        vector<Particle> looseElectrons;
        vector<Particle> looseMuons;
        foreach( const Particle& p, leptons.particlesByPt() )
        {
            double pT = abs( p.momentum().pT() );
            double absEta = abs( p.momentum().eta() );
            if( isParticleType( p, ELECTRON ) && pT > MIN_PT_GOOD_ELECTRON && absEta < MAX_ETA_GOOD_ELECTRON )
            {
                goodElectrons.push_back( p );
            }
            else if( isParticleType( p, MUON ) && pT > MIN_PT_GOOD_MUON && absEta < MAX_ETA_GOOD_MUON )
            {
                goodMuons.push_back( p );
            }
            if( isParticleType( p, ELECTRON ) && pT > MIN_PT_LOOSE_ELECTRON && absEta < MAX_ETA_LOOSE_ELECTRON )
            {
                looseElectrons.push_back( p );
            }
            else if( isParticleType( p, MUON ) && pT > MIN_PT_LOOSE_MUON && absEta < MAX_ETA_LOOSE_MUON )
            {
                looseMuons.push_back( p );
            }
        }
        // Good Lepton Cut
        bool goodElectron = goodElectrons.size()==1 && goodMuons.size() == 0;
        bool goodMuon = goodMuons.size()==1 && goodElectrons.size() == 0;
        bool goodLepton = goodElectron | goodMuon;
        if( goodLepton )
        {
            _cuts[Cuts::GoodLepton]++;
        }
        if( goodElectron )
        {
            _cuts[Cuts::GoodLepton_Electron]++;
        }
        if( goodMuon )
        {
            _cuts[Cuts::GoodLepton_Muon]++;
        }
        // loose Lepton Veto
        isE = goodElectrons.size() == 1 && looseElectrons.size() == 1 && looseMuons.size() == 0;
        isMu = goodMuons.size() == 1 && looseElectrons.size() == 0 && looseMuons.size() == 1;
        bool isSelected = isE ^ isMu;
        if( !isSelected )
        {
            return false;
        }
        if( isE )
        {
            _cuts[Cuts::LooseLeptonVeto_Electron]++;
            chosenLepton = goodElectrons[0];
        }
        if( isMu )
        {
            _cuts[Cuts::LooseLeptonVeto_Muon]++;
            chosenLepton = goodMuons[0];
        }
        return true;
    }

    bool analyze_CountJets( const Event& event, const Particle chosenLepton )
    {
        vector<FourMomentum> jets;
        int nBJets( 0 );
        int nJetsWrongEta( 0 );
        int nJetsTooCloseToGoodLepton( 0 );
        int nJets ( 0 );
        foreach( const Jet& j, applyProjection<FastJets>( event, "Jets" ).jetsByPt( JET_MIN_PT ) )
        {
            const double absEta = abs( j.momentum().eta() );
            if( absEta > JET_MAX_ETA )
            {
                nJetsWrongEta++;
                continue;
            }
            if( deltaR( chosenLepton.momentum(), j.momentum() ) < JET_MIN_DELTA_R )
            {
                auto reducedMomentum = j.momentum()-chosenLepton.momentum();
                if( reducedMomentum.pT() <= JET_MIN_PT )
                {
                    nJetsTooCloseToGoodLepton++;
                    continue;
                }
            }
            if( j.containsBottom() )
            {
                nBJets++;
            }
            nJets++;
        }
        _root->nJetsTooCloseToGoodLepton->Fill( nJetsTooCloseToGoodLepton );
        _root->nJetsWrongEta->Fill( nJetsWrongEta );
        if( nJets < 4 )
        {
            _root->nJetsLessThan4->Fill( nJets );
//             return false;
        }
        _cuts[Cuts::NJetsGE4]++;
        if( isE )
        {
            _cuts[Cuts::NJetsGE4_Electron] ++;
        }
        if( isMu )
        {
            _cuts[Cuts::NJetsGE4_Muon] ++;
        }
        _root->nBJets->Fill( nBJets );
        if( nBJets < 2 )
        {
//       cout << "nBJets veto triggered, but ignored. " << endl;
            //return false;
        }
        _root->nJets->Fill( min( 10,nJets ) );
        return true;
    }

    struct ChiMasses
    {
        ChiMasses() : chiHadW2(-1), chiLepT2(-1), chiHadT2(-1), massLepW(-1), massHadW(-1), massLepT(-1), massHadT(-1) {}
        double chiHadW2, chiLepT2, chiHadT2;
        double massLepW, massHadW, massLepT, massHadT;
        void getChis(double& chiHadW, double& chiLepT, double& chiHadT)
        {
            chiHadW = sqrt(chiHadW2);
            chiLepT = sqrt(chiLepT2);
            chiHadT = sqrt(chiHadT2);
        }
    };

    double analyze_calcChi2( FourMomentum const& p4_Lep_W, FourMomentum const& p4_Lep_B,  FourMomentum const& p4_Lep_T,
                             FourMomentum const& p4_Had_W, FourMomentum const& p4_Had_B,  FourMomentum const& p4_Had_T,
                             ChiMasses& chiMasses )
    {
        double deltaHadW = p4_Had_W.mass() - RECO_MASS_W_HAD;
        double deltaLepT = p4_Lep_T.mass() - RECO_MASS_T_LEP;
        double deltaHadT = p4_Had_T.mass() - RECO_MASS_T_HAD;
        double chiHadW2 = deltaHadW * deltaHadW / RECO_MASS_SIGMA_W_HAD2;
        double chiLepT2 = deltaLepT * deltaLepT / RECO_MASS_SIGMA_T_LEP2;
        double chiHadT2 = deltaHadT * deltaHadT / RECO_MASS_SIGMA_T_HAD2;
        double chi2 = chiHadW2 + chiLepT2 + chiHadT2;
        chiMasses.chiHadW2 = chiHadW2;
        chiMasses.chiLepT2 = chiLepT2;
        chiMasses.chiHadT2 = chiHadT2;

        chiMasses.massLepW = p4_Lep_W.mass();
        chiMasses.massHadW = p4_Had_W.mass();
        chiMasses.massLepT = p4_Lep_T.mass();
        chiMasses.massHadT = p4_Had_T.mass();

        return chi2;
    }

    void displayFourMomentumInfo( const char* name, FourMomentum const& fm )
    {
        cout << name << ": [E= " << fm.E()/GeV << "] [pT=" << fm.pT()/GeV << "] [m=" << fm.mass()/GeV << "]" << endl;
    }

    void analyze_doJetReco( const Event& event, const IdentifiedFinalState& leptons, const Particle chosenLepton )
    {
#ifdef JET_RECO_DEBUG_OUTPUT
        cout << "--- analyze_doJetReco ---" << endl;
#endif
        FourMomentum p4_Lep_W_Nu;
        FourMomentum p4_Lep_W_L = chosenLepton.momentum();
        FourMomentum p4_Lep_W;
        FourMomentum p4_Lep_B;
        FourMomentum p4_Lep_T;
        FourMomentum p4_Had_W_Q1;
        FourMomentum p4_Had_W_Q2;
        FourMomentum p4_Had_B;
        FourMomentum p4_Had_W;
        FourMomentum p4_Had_T;
        int neededNeutrino( 0 );
        bool chosenLeptonIsAnti( chosenLepton.pdgId() < 0 );
        int neutrinoAntiFactor( chosenLeptonIsAnti ? 1 : -1 );
        if( isParticleType( chosenLepton, ELECTRON ) )
        {
            neededNeutrino = NU_E * neutrinoAntiFactor;
        }
        else if( isParticleType( chosenLepton, MUON ) )
        {
            neededNeutrino = NU_MU * neutrinoAntiFactor;
        }
        else if( isParticleType( chosenLepton, TAU ) )
        {
            neededNeutrino = NU_TAU * neutrinoAntiFactor;
        }
        bool found = false;
        double wLepMass = 0;
        foreach( const Particle& p, leptons.particlesByPt() )
        {
            if( p.pdgId() == neededNeutrino )
            {
                found = true;
                // always choose neutrino with highest energy
#ifdef JET_RECO_DEBUG_OUTPUT
                cout << "found 1 matching Neutrino" << endl;
#endif
                if( p.momentum().E() > p4_Lep_W_Nu.E() )
                {
                    p4_Lep_W_Nu = p.momentum();
                    wLepMass = (p4_Lep_W_Nu + p4_Lep_W_L).mass();
#ifdef JET_RECO_DEBUG_OUTPUT
                    cout << "mWLep=" << wLepMass/GeV << endl;

#endif
                }
            }
        }
        if( not found )
        {
#ifdef JET_RECO_DEBUG_OUTPUT
            cout << "No matching neutrino has been found. <return>" << endl;
#endif
            return;
        }
#ifdef JET_RECO_DEBUG_OUTPUT
        else
        {
            cout << "Matching Neutrino: [PdgId=" << neededNeutrino << "] [E=" << p4_Lep_W_Nu.E()/GeV << "] [pT=" << p4_Lep_W_Nu.pT()/GeV << "]" << endl;
        }
#endif

        if (wLepMass < 40*GeV)
        {
#ifdef JET_RECO_DEBUG_OUTPUT
            cout << "cutting invalid m_W_lep < 40 GeV" << endl;
#endif
            return;
        }

        p4_Lep_W = p4_Lep_W_Nu + p4_Lep_W_L;
        vector<FourMomentum> jetsP4;
        foreach( const Jet& j, applyProjection<FastJets>( event, "Jets" ).jetsByPt( JET_RECO_MIN_PT ) )
        {
            if( deltaR( chosenLepton.momentum(), j.momentum() ) < JET_MIN_DELTA_R )
            {
                auto reducedMomentum = j.momentum()-chosenLepton.momentum();
                if( reducedMomentum.pT() > JET_RECO_MIN_PT )
                {
                    jetsP4.push_back( reducedMomentum );
                }
            }
            else
            {
                jetsP4.push_back( j.momentum() );
            }
        }
        int jetsSize( jetsP4.size() );
#ifdef JET_RECO_DEBUG_OUTPUT
        cout << "n jet momenta being searched: " << jetsSize << endl;
#endif
        double chi2( DBL_MAX );
        int additionalJets( 0 );
        ChiMasses chiMassesBest;
        for( int i = 0; i < jetsSize; i++ )
        {
            for( int j = 0; j < jetsSize-1; j++ )
            {
                for( int k = 0; k < jetsSize-2; k++ )
                {
                    for( int l = 0; l < jetsSize-3; l++ )
                    {
                        auto availableJets = jetsP4;
                        p4_Lep_B = availableJets[i];
                        p4_Lep_T = p4_Lep_B + p4_Lep_W;
                        availableJets.erase( availableJets.begin() + i );
                        p4_Had_W_Q1 = availableJets[j];
                        availableJets.erase( availableJets.begin() + j );
                        p4_Had_W_Q2 = availableJets[k];
                        p4_Had_W = p4_Had_W_Q1 + p4_Had_W_Q2;
                        availableJets.erase( availableJets.begin() + k );
                        p4_Had_B = availableJets[l];
                        p4_Had_T = p4_Had_B + p4_Had_W;
                        availableJets.erase( availableJets.begin() + l );
                        ChiMasses chiMasses;
                        double thisChi2 = analyze_calcChi2( p4_Lep_W, p4_Lep_B,  p4_Lep_T, p4_Had_W, p4_Had_B, p4_Had_T, chiMasses );
                        if( thisChi2 > chi2 )
                        {
                            continue;
                        }
                        chiMassesBest = chiMasses;
                        chi2 = thisChi2;
                        int nAddJets (0) ;
                        if (nAddJets != 0)
                        {
                            cout << "nAddJets="  << nAddJets << endl;
                        }
                        foreach( const FourMomentum& j, availableJets )
                        {
//              cout << abs(j.pT()) / GeV << endl;
                            if( abs( j.pT() ) > JET_MIN_PT && abs( j.eta() ) < JET_MAX_ETA )
                            {
                                nAddJets ++;
//                cout << "++" << endl;
#ifdef JET_RECO_DEBUG_OUTPUT
                                //cout << "jet matches min pT: " << abs( j.pT() ) << endl;
#endif
                            }
#ifdef JET_RECO_DEBUG_OUTPUTchi
                            else
                            {
                                cout << "jet pT BELOW threshold: " << abs( j.pT() ) << endl;
                            }
#endif
                        }
                        additionalJets = min( 2,nAddJets );
                    }
                }
            }
        }
        double chi( sqrt( chi2 ) );

        double chiHadW, chiLepT, chiHadT;
        chiMassesBest.getChis(chiHadW, chiLepT, chiHadT);

#ifdef JET_RECO_DEBUG_OUTPUT
        cout << "Result:" << endl;
        displayFourMomentumInfo( "p4_Lep_W_Nu",p4_Lep_W_Nu );
        displayFourMomentumInfo( "p4_Lep_W_L",p4_Lep_W_L );
        displayFourMomentumInfo( "p4_Lep_W",p4_Lep_W );
        displayFourMomentumInfo( "p4_Lep_B",p4_Lep_B );
        displayFourMomentumInfo( "p4_Lep_T",p4_Lep_T );
        displayFourMomentumInfo( "p4_Had_W_Q1",p4_Had_W_Q1 );
        displayFourMomentumInfo( "p4_Had_W_Q2",p4_Had_W_Q2 );
        displayFourMomentumInfo( "p4_Had_B",p4_Had_B );
        displayFourMomentumInfo( "p4_Had_W",p4_Had_W );
        displayFourMomentumInfo( "p4_Had_T",p4_Had_T );
        cout << "---" << endl;
        cout << "chi=" << chi << endl;
        cout << "additionalJets: " << additionalJets << endl;
        cout << "--- end ---" << endl << endl;
#endif

        if (chi < 5)
        {
            _root->additionalJets_chiCut->Fill( additionalJets );
        }

        _root->chi->Fill( chi );
        _root->additionalJets->Fill( additionalJets );
        _root->chiHadW->Fill ( chiHadW );
        _root->chiLepT->Fill ( chiLepT );
        _root->chiHadT->Fill ( chiHadT );
        _root->massLepW->Fill ( chiMassesBest.massLepW );
        _root->massHadW->Fill ( chiMassesBest.massHadW );
        _root->massLepT->Fill ( chiMassesBest.massLepT );
        _root->massHadT->Fill ( chiMassesBest.massHadT );
    }

public:

    void analyze( const Event& event )
    {
        _cuts[Cuts::TotalEvents]++;
        const IdentifiedFinalState leptons = applyProjection<IdentifiedFinalState>( event, "Leptons" );
        Particle chosenLepton;
        if( !analyze_Lepton( event, leptons, chosenLepton ) )
        {
//       cout << "analyze_Lepton veto triggered, but ignored. " << endl;
            //vetoEvent;
        }
        _cuts[Cuts::LooseLeptonVeto]++;
        if( !analyze_CountJets( event, chosenLepton ) )
        {
            //vetoEvent;
        }
        _cuts[Cuts::NBJetsGE2]++;
        if( isE )
        {
            _cuts[Cuts::NBJetsGE2_Electron] ++;
        }
        if( isMu )
        {
            _cuts[Cuts::NBJetsGE2_Muon] ++;
        }
        analyze_doJetReco( event,leptons,chosenLepton );
    }

    void finalize_Root()
    {
        delete _root;
    }

    void finalize_Names()
    {
        for( size_t i = 0; i<Cuts::Size; i++ )
        {
            delete [] _cutNames[i];
        }
        delete [] _cutNames;
        delete [] _cuts;

    }

    void finalize_DisplayCuts()
    {
        int count( -1 );
        int countIndex( 0 );
        int childCount( 0 );
        cout << "[Cut flow]" << endl;
        cout << "--------------------------------------------------------" << endl;
        cout << "[#.#] (Cut Descr.|Branch):nEvents ( Parent% /   Total% )" << endl;
        cout << "--------------------------------------------------------" << endl;
        for( size_t i=0; i<Cuts::Size; i++ )
        {
            double percentage = 100.*_cuts[i]/_cuts[Cuts::TotalEvents];
            double ofParentPercentage = 100. * _cuts[i]/_cuts[countIndex];
            cout.precision( 3 );
            bool isChild = false;
            if( std::string( _cutNames[i] ).find( '|' ) != std::string::npos )
            {
                isChild = true;
            }
            if( !isChild )
            {
                count++;
                countIndex = i;
            }
            cout << "[" << count;
            if( isChild )
            {
                cout << "." << childCount;
            }
            else
            {
                cout << "  ";
            }
            cout << "] (" << _cutNames[i] << "): " << setw( 6 ) << _cuts[i] << " (" << setw( 7 ) << ofParentPercentage << "% / " << setw( 7 ) << percentage << "% )" << endl;
            if( !isChild )
            {
                childCount=0;
            }
            else
            {
                childCount++;
            }
        }
        cout << "--------------------------------------------------------" << endl;
    }

    void finalize_DisplayJetMultiplicy()
    {
        cout << "[Jet Multiplicy]" << endl;
        for( int i=4; i<=10; i++ )
        {
            cout << "[" << i << "]: " << _root->nJets->GetBinContent( i-3 ) << " +- " << _root->nJets->GetBinError( i-3 ) << endl;
        }
    }

    void finalize_DisplayResults()
    {
        cout << endl;
        cout << "[Displaying Results]" << endl;
        cout << endl;
        finalize_DisplayCuts();
        cout << endl;
        finalize_DisplayJetMultiplicy();
        cout << endl;
    }

    void finalize()
    {
        finalize_DisplayResults();
        finalize_Root();
    }



};

AnalysisBuilder<TTBarLJet_KITA> plugin_TTBarLJet_KITA;
}

