#!/bin/sh

cmsRun test/runRivetAnalyzer_cfg1.py
mv analysis.root analysis1.root
cmsRun test/runRivetAnalyzer_cfg2.py
mv analysis.root analysis2.root
cmsRun test/runRivetAnalyzer_cfg3.py
mv analysis.root analysis3.root
cmsRun test/runRivetAnalyzer_cfg4.py
mv analysis.root analysis4.root
hadd analysis.root analysis1.root analysis2.root analysis3.root analysis4.root